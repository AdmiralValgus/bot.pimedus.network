import pyowm
from time import time
import datetime

class Weather(object):
    def __init__(self,api_key,target):
        # Api key for https://openweathermap.org
        owm = pyowm.OWM(api_key)
        observation = owm.weather_at_place(target)
        self.weather = observation.get_weather()
        #Get location from api to verify we are looking at the right place
        location = observation.get_location()
        self.city = location.get_name()
        self.country = location.get_country()

    def temp(self):
        return self.weather.get_temperature('celsius')['temp']

    def wind(self):
        return self.weather.get_wind('meters_sec')['speed']

    def sunset(self,):
        suntime = self.weather.get_sunset_time()
        return(datetime.datetime.fromtimestamp(suntime).strftime('%H:%M:%S'))

    def sunrise(self,):
        suntime = self.weather.get_sunrise_time()
        return(datetime.datetime.fromtimestamp(suntime).strftime('%H:%M:%S'))

    def until_sunset(self,):
        suntime = self.weather.get_sunset_time() - int(time())
        return(datetime.datetime.fromtimestamp(suntime).strftime('%H:%M:%S'))

    def sunshine(self,):
        suntime = int(time()) - self.weather.get_sunrise_time()
        return(datetime.datetime.fromtimestamp(suntime).strftime('%H:%M:%S'))

if __name__ == '__main__':
    # Example
    w = Weather('Tartu')
    print('%s, %s'%(w.location.get_name(),w.location.get_country()))
    print('Temperature: %s c'%w.temp())
    print('Wind: %s m/s'%w.wind())
    print('Sun sets at %s, there is still %s left'%(w.sunset(),w.until_sunset()))
    print('Sun rose at %s, and has been up for %s'%(w.sunrise(),w.sunshine()))