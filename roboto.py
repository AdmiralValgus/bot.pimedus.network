import random
import configparser

from matrix_bot_api.matrix_bot_api import MatrixBotAPI
from matrix_bot_api.mregex_handler import MRegexHandler
from matrix_bot_api.mcommand_handler import MCommandHandler

from addons.weather import Weather
from addons.ddgsearch import search

config = configparser.ConfigParser()
config.read('config.ini')

USERNAME = config['BOT']['USERNAME']
PASSWORD = config['BOT']['PASSWORD']
SERVER   = config['BOT']['SERVER']
API_KEY  = config['WEATHER']['API_KEY']

def hi_callback(room, event):
    # Somebody said hi, let's say Hi back
    room.send_text("Hi, " + event['sender'])

def weather_callback(room, event):
    try:
        place = event['content']['body'].split('!ilm ')[1]
        w = Weather(API_KEY,place)
        message = '%s,%s: Temperatuur:%sC Tuul:%sm/s' % (w.city,w.country, w.temp(),w.wind())
    except Exception as e:
        print(e)
        message = 'Ei saanud aru. Korrektne süntaks: !ilm koht (!ilm Tartu, !ilm Narva, !ilm Shanghai)'
    room.send_text(message)

def search_callback(room, event):
    search_keyword = event['content']['body'].split('!otsi ')[1]
    message = search(search_keyword)
    room.send_text(message)

def main():

    # Create an instance of the MatrixBotAPI
    bot = MatrixBotAPI(USERNAME, PASSWORD, SERVER,debug=True)

    weather_handler = MCommandHandler("ilm", weather_callback)
    bot.add_handler(weather_handler)

    search_handler = MCommandHandler("otsi", search_callback)
    bot.add_handler(search_handler)

    # Add a regex handler waiting for the word Hi
    hi_handler = MRegexHandler("Hi", hi_callback)
    bot.add_handler(hi_handler)
    # Start polling
    bot.start_polling()

    # Infinitely read stdin to stall main thread while the bot runs in other threads
    while True:
        #print(bot.rooms)
        input()


if __name__ == "__main__":
    main()
