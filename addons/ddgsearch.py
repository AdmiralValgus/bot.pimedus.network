from duckduckpy import query

def search(keyword):
    response = query(keyword)
    try:
        return response.related_topics[0].text
    except Exception as e:
        return False